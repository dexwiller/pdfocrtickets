import configparser
import json

config = configparser.ConfigParser()
config.read('config.ini', encoding="utf-8")

delete_main_files = config['APP'].getboolean('delete_main_files')
img_show = config['APP'].getboolean('img_show')
img_show_cropped = config['APP'].getboolean('img_show_cropped')
log_opts = config['APP']['log_level']
ocr_options = config['OCR']['ocr_options']
index_map = json.loads(str(config['OCR']['index_map']))
size_map = json.loads(str(config['OCR']['size_map']))
index_map_2 = json.loads(str(config['OCR']['index_map_2']))
size_map_2 = json.loads(str(config['OCR']['size_map_2']))
rule_set = json.loads(str(config['OCR']['rule_set']))
ftp_host = config['FTP']['host']
ftp_port = int(config['FTP']['port'])
ftp_user = config['FTP']['user']
ftp_pass = config['FTP']['pass']
delete_temp_files = config['APP'].getboolean('delete_temp_files')
db_host = config['DB']['host']
db_port = config['DB']['port']
db_user = config['DB']['user']
db_pass = config['DB']['pass']
db_schema = config['DB']['schema']
db_db = config['DB']['db']
db_table = config['DB']['table']
sum_table = config['DB']['sum_table']
