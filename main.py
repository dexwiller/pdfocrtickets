from pathlib import Path

from PIL import Image
import pytesseract
import cv2
import imutils
from pdf2image import convert_from_path
import fitz
import numpy as np
from config import size_map, index_map, delete_main_files, ocr_options, size_map_2, index_map_2, img_show, \
    img_show_cropped
import util
from calendar import monthrange

from logger import pylogger

DESIRED_INCREASE = 300 / 72
mat = fitz.Matrix(DESIRED_INCREASE, DESIRED_INCREASE)


class OcrOps:
    def __init__(self, global_path):
        self.path = global_path
        self.image = None
        self.page_indexes = {}

    def remove_noise_and_smooth(self, img):
        # img = cv2.cvtColor(cropped, cv2.COLOR_BGR2GRAY)
        filtered = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 9, 41)
        kernel = np.ones((1, 1), np.uint8)
        opening = cv2.morphologyEx(filtered, cv2.MORPH_OPEN, kernel)
        closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
        img = self.image_smoothening(img)
        or_image = cv2.bitwise_or(img, closing)
        return or_image

    def image_smoothening(self, cropped):

        ret1, th1 = cv2.threshold(cropped, 88, 255, cv2.THRESH_BINARY)
        ret2, th2 = cv2.threshold(th1, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        blur = cv2.GaussianBlur(th2, (5, 5), 0)
        ret3, th3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        return th3

    def remove_lines(self, im):
        fld = cv2.ximgproc.createFastLineDetector(20, 5, 300, 450, 3, 1)
        lines = fld.detect(im)

        for line in lines:
            x1, y1, x2, y2 = line[0]
            if abs(y1 - y2) > 75:
                cv2.line(im, (int(x1), int(y1)), (int(x2), int(y2)), (255, 255, 255), 10)
            if abs(x1 - x2) > 75:
                cv2.line(im, (int(x1), int(y1)), (int(x2), int(y2)), (255, 255, 255), 15)

        return im

    def correct_skew_0(self):
        # ret, thresh = cv2.threshold(self.image, 10, 255, cv2.THRESH_BINARY_INV)

        # gray = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(self.image, (5, 5), 0)
        edged = cv2.Canny(gray, 10, 50)
        # Find the contours
        contours = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

        x, y, r, b = [self.image.shape[1] / 2, self.image.shape[0] / 2, 0, 0]
        # Iterate over all the contours, and keep updating the bounding rect
        contours = contours[1] if imutils.is_cv3() else contours[0]
        contours = sorted(contours, key=cv2.contourArea, reverse=True)[:5]
        for cnt in contours:

            rect = cv2.boundingRect(cnt)
            if rect[0] < x:
                x = rect[0]
            if rect[1] < y:
                y = rect[1]
            if rect[0] + rect[2] > r:
                r = rect[0] + rect[2]
            if rect[1] + rect[3] > b:
                b = rect[1] + rect[3]

        bounding_rect = np.array([(x, y),
                                  (x + r, y),
                                  (x, y + b),
                                  (x + r, y + b)])
        # Debugging Purpose.
        if x > 200 or y > 200:
            return []
        cv2.rectangle(self.image, (x, y), (r, b), (0, 0, 0), 2)
        return self.four_point_transform(bounding_rect)

    def correct_skew_1(self, page, second_process=False, format='old'):
        # gray = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
        if format == 'new':
            page = 2
        ref_size = size_map.get(str(page))
        if second_process:
            ref_size = size_map_2.get(str(page))
        (h1, w1) = self.image.shape[:2]
        if h1 < ref_size[0] or w1 < ref_size[1]:
            return True
        gray = cv2.GaussianBlur(self.image, (5, 5), 0)
        edged = cv2.Canny(gray, 10, 50)
        cnts = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
        # print ( cnts )
        cnts = cnts[1] if imutils.is_cv3() else cnts[0]
        cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:5]
        screen_cnt = []
        for c in cnts:
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.02 * peri, True)
            if len(approx) == 4:
                screen_cnt = approx
                break

        if len(screen_cnt) > 0:
            cv2.drawContours(self.image, [screen_cnt], -1, (0, 255, 0), 2)
            # process_height = self.image.
            ratio = 1  # self.image.shape[0] / float(process_height)
            pts = np.array(screen_cnt.reshape(4, 2) * ratio)
            warped = self.four_point_transform(pts)
            (h, w) = warped.shape[:2]
            # (h, w, ref_size)
            if h < ref_size[0] or w < ref_size[1]:
                warped = self.correct_skew_fail_over(ref_size)
        else:
            warped = self.correct_skew_fail_over(ref_size)

        if len(warped) > 0:
            if img_show:
                cv2.imshow("aa", self.image)
                cv2.imshow("bb", warped)
                cv2.waitKey(0)
            self.image = warped

    def correct_skew_fail_over(self, ref_size):
        warped = self.correct_skew_0()
        (h, w) = (0, 0)
        if len(warped) > 0:
            (h, w) = warped.shape[:2]
        if h < ref_size[0] or w < ref_size[1]:
            warped = self.correct_skew_2()
            return warped
        return []

    def correct_skew_2(self):
        self.image = self.correct_skew_3()
        # gray = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
        gray = cv2.bitwise_not(self.image)
        thresh = cv2.threshold(gray, 0, 255,
                               cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
        coords = np.column_stack(np.where(thresh > 0))
        angle = cv2.minAreaRect(coords)[-1]
        if angle < -45:
            angle = -(90 + angle)
        else:
            angle = -angle
        (h, w) = self.image.shape[:2]
        center = (w // 2, h // 2)
        M = cv2.getRotationMatrix2D(center, angle, 1.0)
        rotated = cv2.warpAffine(self.image, M, (w, h),
                                 flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)
        # print("[INFO] angle: {:.3f}".format(angle))
        if img_show:
            cv2.imshow("Input", self.image)
            cv2.imshow("Rotated", rotated)
            cv2.waitKey(0)
        return rotated

    def correct_skew_3(self):
        image = self.image
        original = image.copy()
        # gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        blur = cv2.GaussianBlur(self.image, (25, 25), 0)
        thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

        # Perform morph operations, first open to remove noise, then close to combine
        noise_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, noise_kernel, iterations=2)
        close_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
        close = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, close_kernel, iterations=3)

        # Find enclosing boundingbox and crop ROI
        coords = cv2.findNonZero(close)
        x, y, w, h = cv2.boundingRect(coords)
        cv2.rectangle(image, (x, y), (x + w, y + h), (36, 255, 12), 2)
        crop = original[y:y + h, x:x + w]
        return crop

    def final_crop(self):

        original = self.image.copy()
        blur = cv2.GaussianBlur(self.image, (25, 25), 0)
        thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

        # Perform morph operations, first open to remove noise, then close to combine
        noise_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, noise_kernel, iterations=2)
        close_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
        close = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, close_kernel, iterations=3)

        # Find enclosing boundingbox and crop ROI
        coords = cv2.findNonZero(close)
        x, y, w, h = cv2.boundingRect(coords)
        cv2.rectangle(self.image, (x, y), (x + w, y + h), (36, 255, 12), 2)
        crop = original[y:y + h, x:x + w]
        self.image = crop

    def order_points(self, pts):

        # initialzie a list of coordinates that will be ordered
        # such that the first entry in the list is the top-left,
        # the second entry is the top-right, the third is the
        # bottom-right, and the fourth is the bottom-left
        rect = np.zeros((4, 2), dtype="float32")

        # the top-left point will have the smallest sum, whereas
        # the bottom-right point will have the largest sum
        s = pts.sum(axis=1)
        rect[0] = pts[np.argmin(s)]
        rect[2] = pts[np.argmax(s)]

        # now, compute the difference between the points, the
        # top-right point will have the smallest difference,
        # whereas the bottom-left will have the largest difference
        diff = np.diff(pts, axis=1)
        rect[1] = pts[np.argmin(diff)]
        rect[3] = pts[np.argmax(diff)]

        # return the ordered coordinates
        return rect

    def four_point_transform(self, pts):

        rect = self.order_points(pts)
        (tl, tr, br, bl) = rect
        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))

        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))

        dst = np.array([
            [0, 0],
            [maxWidth - 1, 0],
            [maxWidth - 1, maxHeight - 1],
            [0, maxHeight - 1]], dtype="float32")

        # compute the perspective transform matrix and then apply it
        matrix = cv2.getPerspectiveTransform(rect, dst)
        warped = cv2.warpPerspective(self.image, matrix, (maxWidth, maxHeight))

        return warped

    def pdf2image(self):
        # for path in Path(self.path).rglob('*.pdf'):
        with convert_from_path(self.path, 300) as pages:
            image_counter = 0
            for page in pages:
                filename = "{}_page_{}.png".format(self.path, image_counter)
                # Save the image of the page in system
                page.save(filename, 'PNG')
                util.set_image_dpi(filename)
                image = cv2.imread(filename)
                self.image = image
                self.correct_skew()
                # self.get_image_fields(filename, image_counter)
                # print('-------------------------------------------------------------')
                image_counter += 1
                util.remove_temp_files(filename, delete_main_files)

    def pdf2image_fitz(self):
        from datetime import datetime as dt
        # print('start: ', dt.now())
        # for path in Path(self.path).rglob('*.pdf'):
        f_res = {}
        with fitz.open(self.path) as doc:
            base_text = doc.getPageText(0)
            format = 'old'
            if base_text.upper().find('TAX') >= 0:
                format = 'new'
                doc.deletePage(0)

            for i in range(doc.pageCount):
                page = doc.loadPage(i)
                annot = page.firstAnnot  # first annotation
                while annot:  # as long as there are any ...
                    annot = page.deleteAnnot(annot)  # delete them
                pix = page.getPixmap(matrix=mat, alpha=False)  # now make pixmap
                # alpha=False is important for runtime and size savings
                filename = "page_" + str(i) + ".png"
                # Save the image of the page in system
                pix.writePNG(filename)

                res = {}
                if i == 0:
                    new_path, res = self.ready_1(filename, True, i)
                    if not new_path and not res:
                        continue
                    if not res.get('Province') or not res.get('LicensePlate'):
                        new_path, res = self.ready_1(filename, False, i, True, res)
                        if not new_path and not res:
                            continue
                    f_res.update(res)
                    util.remove_temp_files(new_path, delete_main_files)
                elif i == 1:

                    self.image = cv2.imread(filename, 0)
                    continue_ = self.correct_skew_1(i)
                    print(continue_)
                    if continue_:
                        continue
                    (h, w) = self.image.shape[:2]
                    half = int(h / 4) + 20
                    self.image = self.crop_image(0, 0, half, w)
                    self.remove_lines(self.image)
                    self.final_crop()
                    if img_show:
                        cv2.imshow('image.png', self.image)
                        cv2.waitKey(0)

                    f_res.update(self.get_image_fields(i))

                # print('--------------PAGE {}----------------'.format(i))
                util.remove_temp_files(filename, delete_main_files)

            # print(f_res)
            date = f_res.get('date') or '01.01.1900'
            time = f_res.get('time') or '00:00'
            try:
                day_mount_year = date.split('.')
                day = int(day_mount_year[0])
                month = int(day_mount_year[1])
                year = int(day_mount_year[2])
                if month > 12:
                    month = 12
                if day > monthrange(year, month)[1]:
                    day = monthrange(year, month)[1]
                date = '{}.{}.{}'.format(year, month, day)
            except Exception as e:
                pylogger.error('Date Error: {}'.format(e))
                date = '1900.01.01'

            try:
                time_s = time.split(':')
                if int(time_s[0]) >= 24:
                    time_s[0] = '00'
                if int(time_s[1]) >= 60:
                    time_s[1] = '00'

                time = '{}:{}'.format(time_s[0], time_s[1])
            except Exception as e:
                pylogger.error('Time Error: {}'.format(e))
                time = '00:00'

            fine_date = '{} {}'.format(date, time)
            f_res.update({'FineDate': fine_date})
            if 'date' in f_res:
                del (f_res['date'])
            if 'time' in f_res:
                del (f_res['time'])
            # print(f_res)

            # print('end: ', dt.now())

            return f_res

    def ready_1(self, filename, refactor, index, second_process=False, previous_res={}):
        new_path = util.set_image_dpi(filename, refactor)
        self.image = cv2.imread(new_path, 0)
        (h, w) = self.image.shape[:2]
        half = int(h / 2) + 20
        self.image = self.crop_image(0, 0, half, w)
        continue_ = self.correct_skew_1(index, second_process)
        if continue_:
            return False, False
        self.final_crop()
        res = self.get_image_fields(index, second_process, previous_res=previous_res)
        return new_path, res

    def call_ocr_and_normalize(self, cropped, i):
        text = self.ocr_image(cropped, custom_options=' -c tessedit_char_whitelist="{}"'.format(
            self.page_indexes[str(i)]['whitelist']))
        normalized_text = util.normalize_str(text, i)
        return normalized_text

    def get_image_fields(self, page_num, second_process=False, smoothening=False, previous_res={}):
        return_dict = {}

        self.page_indexes = index_map.get(str(page_num)) or []
        if second_process:
            self.page_indexes = index_map_2.get(str(page_num)) or []
        for i in self.page_indexes:
            if not str(i) in previous_res:
                cropped = self.crop_image(**self.page_indexes[str(i)])
                if smoothening:
                    cropped = self.remove_noise_and_smooth(cropped)

                    if img_show_cropped:
                        win_name = "Smooth"
                        cv2.namedWindow(win_name)  # Create a named window
                        cv2.moveWindow(win_name, 60, 30)  # Move it to (40,30)
                        cv2.imshow(win_name, cropped)
                        cv2.waitKey(0)
                normalized_text = self.call_ocr_and_normalize(cropped, i)
                if not normalized_text:
                    cropped = cv2.resize(cropped, None, fx=2, fy=2, interpolation=cv2.INTER_CUBIC)
                    normalized_text = self.call_ocr_and_normalize(cropped, i)
                    if img_show_cropped:
                        win_name = "Smooth"
                        cv2.namedWindow(win_name)  # Create a named window
                        cv2.moveWindow(win_name, 60, 30)  # Move it to (40,30)
                        cv2.imshow(win_name, cropped)
                        cv2.waitKey(0)
                return_dict.update(normalized_text)
            else:
                return_dict.update(previous_res)
        return return_dict

    def crop_image(self, x, y, h, w, whitelist=""):

        crop_img = self.image[y:y + h, x:x + w]
        if img_show_cropped:
            win_name = "Cropped"
            cv2.namedWindow(win_name)  # Create a named window
            cv2.moveWindow(win_name, 40, 30)  # Move it to (40,30)
            cv2.imshow(win_name, crop_img)
            cv2.waitKey(0)

        return crop_img

    def ocr_image(self, image, preprocess=None, direct_ocr=True, custom_options=""):
        img_new = Image.fromarray(image)
        custom_ocr_options = '{} {}'.format(custom_options, ocr_options)

        if direct_ocr:
            try:
                return pytesseract.image_to_string(img_new, config=custom_ocr_options)
            except Exception as e:
                pylogger.error('Ocr Error: {}'.format(e))
                return ""
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        if preprocess == 'tresh':
            gray = cv2.threshold(gray, 0, 255,
                                 cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
        elif preprocess == 'blur':
            gray = cv2.medianBlur(gray, 3)
        img_new = Image.fromarray(gray)
        text = pytesseract.image_to_string(img_new, config=ocr_options)

        return text


OcrOps('images/yeni.pdf').pdf2image_fitz()
