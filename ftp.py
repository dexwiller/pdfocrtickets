from ftplib import FTP_TLS, FTP
from main import OcrOps
from config import ftp_host, ftp_port, ftp_pass, ftp_user, delete_temp_files, db_table, sum_table
import ssl
from tempfile import NamedTemporaryFile
from db import ConnectDB
import pysftp
from datetime import datetime
from logger import pylogger
import base64
from pypika import Parameter


class FixedFTPTLS(FTP_TLS):
    """Explicit FTPS, with shared TLS session"""

    def ntransfercmd(self, cmd, rest=None):
        conn, size = FTP.ntransfercmd(self, cmd, rest)
        if self._prot_p:
            conn = self.context.wrap_socket(conn,
                                            server_hostname=self.host,
                                            session=self.sock.session)  # this is the fix
        return conn, size


class FtpConnectFtpLib:

    def retrieve_files(self, path=""):
        f_res = []
        with FixedFTPTLS() as ftp:
            ftp.ssl_version = ssl.PROTOCOL_TLS
            ftp.connect(ftp_host, ftp_port)
            ftp.login(ftp_user, ftp_pass)
            ftp.prot_p()
            f_list = ftp.nlst(path)
            conn = ConnectDB()
            for f_path in f_list:
                if f_path.endswith(".pdf"):
                    f_handle = NamedTemporaryFile(delete=False)
                    f_handle.seek(0)
                    name = f_handle.name
                    ftp.retrbinary('RETR ' + f_path,
                                   f_handle.write)
                    f_handle.flush()

                    values_dict = OcrOps(name).pdf2image_fitz()
                    f_res.append(values_dict)
                    conn.create_insert(values_dict)

                    # if delete_temp_files:
                    #     time.sleep(0.1)
                    # util.remove_temp_files(name, delete_temp_files)
            conn.close_conn()
        return f_res


class FtpConnect:

    def retrieve_files(self, path=""):
        path_first = path
        cnopts = pysftp.CnOpts()
        cnopts.hostkeys = None
        f_res = []
        conn = ConnectDB()
        with pysftp.Connection(host=ftp_host, port=ftp_port, username=ftp_user, password=ftp_pass,
                               cnopts=cnopts) as sftp:

            if path:
                path = '{}/'.format(path)
            data = sftp.listdir()
            t_counter = 0
            f_counter = 0
            f_details = ''
            for files in data:
                if files.endswith('.pdf'):
                    t_counter += 1
                    pylogger.info("File : {}".format(files))
                    f_handle = NamedTemporaryFile(delete=False)
                    f_handle.seek(0)
                    name = f_handle.name
                    sftp.get('{}{}'.format(path, files), name)
                    f_handle.flush()
                    try:
                        values_dict = OcrOps(name).pdf2image_fitz()
                        try:
                            postdate = datetime.strptime(files[:10], '%d.%m.%Y').strftime('%Y.%m.%d')
                        except Exception as e:
                            pylogger.error('PostDate Error: {}'.format(e))
                            postdate = '1900.01.01'
                        values_dict.update({'PostDate': postdate})
                    except Exception as e:
                        pylogger.error('Ocr Error: {}'.format(e))
                        values_dict = {}
                    values_dict.update({'CompanyId': 2, 'FirmaId': 1, 'CreatedOn': Parameter('CURRENT_TIMESTAMP'),
                                        'StatusId': 1, 'CaseTypeId': 1, 'Name': files, 'BrandModel': path_first or ""})

                    with open(name, "rb") as image_file:
                        encoded_string = base64.b64encode(image_file.read()).decode('utf-8')
                        values_dict.update({'Document': encoded_string})

                    try:
                        success = conn.create_insert(values_dict, db_table)
                        if success:
                            delete_ftp = True
                        else:
                            delete_ftp = False
                    except Exception as e:
                        pylogger.error('Sql Error: {}'.format(e))
                        delete_ftp = False
                        conn.close_conn()
                        conn = ConnectDB()
                    # if delete_temp_files:
                    #     time.sleep(0.1)
                    # util.remove_temp_files(name, delete_temp_files)
                    if 'Document' in values_dict:
                        del (values_dict['Document'])
                    if 'CreatedOn' in values_dict:
                        del (values_dict['CreatedOn'])

                    if delete_ftp:
                        sftp.remove('{}{}'.format(path, files))
                        pylogger.info('File Deleted from FTP')
                    else:
                        f_counter += 1
                        f_details += '{},'.format(files)

                    f_res.append(values_dict)

            sum_dict = {'TotalCount': t_counter,
                        'FailedCasesCount': f_counter,
                        'CreatedOn': Parameter('CURRENT_TIMESTAMP'),
                        'FirmaId': 1,
                        'FailedCaseFileDetails': f_details,
                        'IsProcessed': 0}
            try:
                conn.create_insert(sum_dict, sum_table)
            except Exception as e:
                pylogger.error('Summary Sql Error: {}'.format(e))
            conn.close_conn()
            return f_res
