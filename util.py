import os
from PIL import Image
from config import rule_set
import re
import configparser
from datetime import datetime as dt

config = configparser.ConfigParser()
config.read('config.ini', encoding="utf-8")


def normalize_str(text, key):
    text = text.replace('\n', ' ').replace('\f', ' ').replace('\t', ' ').split(" ")
    city_list = rule_set.get('city')
    return_dict = {}
    for t in text:
        if key == 'Province':
            if len(t) >= 3:
                city = [x for x in city_list if t.find(x) > -1]
                if len(city) > 0:
                    return_dict.update({key: city[0]})
        elif key == 'LicensePlate':
            #print(t)
            if len(t) > 5 and not t.isdigit():
                digits = [i for i, c in enumerate(t) if c.isdigit()]
                if len(digits) > 4:  # a plate number contains at least  4 digits.
                    t = t[int(digits[0]):int(digits[-1]) + 1]
                    if len(t) > 5 and not t.isdigit():
                        first_2 = t[0:2]
                        # ocr sometimes mixes A with 4 and B with 8:
                        first_2 = first_2.replace('A', '4').replace('B', '8')
                        if first_2.isdigit():
                            if len(t) > 8:
                                sub_t = t[2:]
                                first_digit = [i for i, c in enumerate(sub_t) if c.isdigit()][
                                    0]  # no need to control cos previous control claims at least 4 digit.
                                last_t = sub_t[first_digit:]
                                if len(last_t) > 4:
                                    last_t = last_t[1:]
                                    t = '{}{}{}'.format(first_2, sub_t[0:first_digit], last_t)
                                else:
                                    t = '{}{}{}'.format(first_2, sub_t[0:first_digit - 1], last_t)

                            return_dict.update({key: t})
        elif key == 'SerialNumber':
            if t.isupper() and len(t) > 1:
                t = re.sub('[^A-Z]', '', t)
                return_dict.update({key: t})
                break
        elif key == 'LineNumber':
            if len([i for i, c in enumerate(t) if c.isdigit()]) > 4:
                t = re.sub('[^0-9]', '', t)
                return_dict.update({key: t})
        elif key == 'FieldMark1':
            if len([i for i, c in enumerate(t) if c.isdigit()]) > 1 and not t.isdigit():
                # handling records like 5112-8 which is correctly 51/2-B
                if t.find('/') == -1:
                    splatted = t.split('-')
                    if len(splatted) > 1:
                        if len(str(splatted[0])) >= 4 and splatted[0][2] != '/':
                            splatted[0] = '{}/{}'.format(splatted[0][:2],
                                                         splatted[0][len(splatted[0]) - 1:len(splatted[0])])
                        elif len(str(splatted[0])) >= 3 and splatted[0][2] != '/':
                            splatted[0] = '{}/{}'.format(splatted[0][:2],
                                                         splatted[0][2:])
                        if str(splatted[1]) == '8':
                            splatted[1] = 'B'
                        t = '{}-{}'.format(splatted[0], splatted[1])


                return_dict.update({key: t})

        elif key == 'FineAmount':
            if len([i for i, c in enumerate(t) if c.isdigit()]) > 1 and t.isdigit():
                return_dict.update({key: t})

        elif key == 'date':

            if len(t) == 10 and (t.count('.') == 2 or t.count('-') == 2 or t.count('/') == 2 or t.count(' ') == 2):
                t = t.replace('-', '.').replace('/', '.').replace(' ', '.')
                return_dict.update({key: t})
                break
            if len(t) >= 8:
                t = t.replace('-', '').replace('/', '').replace(' ', '').replace('.', '')
                if t.isdigit():
                    if int(t[len(t) - 4:len(t)]) >= 2000:
                        t = t[len(t) - 8:len(t)]
                    else:
                        t = t[:8]
                    t = '{}.{}.{}'.format(t[:2], t[2:4], t[4:8])
                    return_dict.update({key: t})

        elif key == 'time':
            # print(t)
            if len(t) == 5 and t.find(':') == 2:
                return_dict.update({key: t})
                break
            elif len(t) > 5 and t.find(':') >= 2:
                t = '{}:{}'.format(t[t.find(':') - 2:t.find(':')], t[t.find(':') + 1:t.find(':') + 3])
                return_dict.update({key: t})
            elif len(t) == 4:
                t = '{}:{}'.format(t[:2], t[2:])
                return_dict.update({key: t})

    return return_dict


def set_image_dpi(file_path, refactor=False, size=False, dpi=(300, 300), replace=False):
    im = Image.open(file_path)
    if not size:
        length_x, width_y = im.size
        factor = 1
        if refactor:
            factor = min(1, float(1024.0 / length_x))
        size = int(factor * length_x), int(factor * width_y)
    # print (size)
    im_resized = im.resize(size, Image.ANTIALIAS)
    new_path = file_path
    if not replace:
        new_path = '{}_1.png'.format(file_path.split('.')[0])

    im_resized.save(new_path, dpi=dpi)
    im.close()

    return new_path


def remove_temp_files(filename, remove=True):
    if remove:
        try:
            if os.path.exists(filename):
                os.remove(filename)
        except Exception as e:
            print("File Can Not Be Deleted -- {}".format(filename))

# def angles(self, image):
#     image = np.mean(image, axis=2)
#     hspace, angles, distances = hough_line(image)
#
#     # Find angle
#     angle = []
#     for _, a, distances in zip(*hough_line_peaks(hspace, angles, distances)):
#         angle.append(a)
#
#     # Obtain angle for each line
#     angles = [a * 180 / np.pi for a in angle]
#
#     # Compute difference between the two lines
#     angle_difference = np.max(angles) - np.min(angles)
#     print(angle_difference)
