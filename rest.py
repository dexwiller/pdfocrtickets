from flask import Flask
from flask_restful import Resource, Api
from ftp import FtpConnect

app = Flask(__name__)
api = Api(app)
api.app.config['RESTFUL_JSON'] = {
    'ensure_ascii': False
}


def global_get(path=""):
    values_dict = FtpConnect().retrieve_files(path)
    return values_dict


class GetOcr(Resource):
    def get(self):
        return global_get()


class GetOcrPath(Resource):
    def get(self, path):
        if path != 'favicon.ico':
            return global_get(path)
        else:
            return ""


api.add_resource(GetOcrPath, '/<path>')
api.add_resource(GetOcr, '/get_ocr_all')
if __name__ == '__main__':
    app.run(debug=True, ssl_context=('cert/cert.pem', 'cert/key.pem'))
