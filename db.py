import pymssql
from config import db_host, db_port, db_user, db_pass, db_db, db_schema
from pypika import Table, Query, Parameter

from logger import pylogger


class ConnectDB:

    def __init__(self):
        self.conn = pymssql.connect(db_host, db_user, db_pass, db_db)
        self.cursor = self.conn.cursor()

    def create_sum_insert(self, values_dict):
        pass

    def create_insert(self, values_dict, table):
        table = Table(table)
        columns = values_dict.keys()
        values = values_dict.values()

        q = Query.into(table).columns(*columns).insert(*values)
        # pylogger.debug('Executed Sql : {}'.format(q))
        try:
            self.cursor.execute(str(q))
            self.conn.commit()
            return True
        except Exception as e:
            pylogger.error('Sql Error: {}'.format(e))
            return False

    def close_conn(self):
        self.conn.close()
