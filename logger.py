import logging
from config import log_opts

logging.basicConfig(filename='log.log',
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=getattr(logging, log_opts))

pylogger = logging.getLogger(__name__)
